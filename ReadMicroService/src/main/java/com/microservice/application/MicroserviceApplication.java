package com.microservice.application;

import com.microservice.configuration.MicroserviceConfiguration;
import com.microservice.module.ApplicationModule;
import com.microservice.resource.ClientResource;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import ru.vyarus.dropwizard.guice.GuiceBundle;
import ru.vyarus.dropwizard.guice.module.installer.feature.jersey.ResourceInstaller;

public class MicroserviceApplication extends Application<MicroserviceConfiguration>{

	public static void main(String[] args) {
		
		try {
			args = new String[]{"server", "src/main/resources/application_LOCAL.yml"};
			new MicroserviceApplication().run(args);
		} catch(Exception exception) {
			exception.printStackTrace();
		}
		System.out.println("Welcome Dropwizard Microservice");
	}

	@Override
	public void run(MicroserviceConfiguration microserviceConfiguration, Environment environment) throws Exception {
		
		environment.jersey().register(new ClientResource());
	}
	
	@Override
	public void initialize(Bootstrap<MicroserviceConfiguration> bootstrap) {

		super.initialize(bootstrap);
		
		 GuiceBundle<MicroserviceConfiguration> guiceBundle = GuiceBundle.<MicroserviceConfiguration>builder()
	                // Jersey resource installer search class of path annotated
	                .installers(ResourceInstaller.class)
	                // it will bind  the model for auto injection
	                .modules(new ApplicationModule(bootstrap))
	                // auto injecting beans(Component)
	                //.extensions(EmployeeResource.class).build();
	                .extensions(ClientResource.class).build();
		 
	        bootstrap.addBundle(guiceBundle);
	        System.out.println("AppMain.initialize");
	}
}
