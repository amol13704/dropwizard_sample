package com.microservice.configuration;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;

public class MicroserviceConfiguration extends Configuration{

	public DataSourceFactory getDatabase() {
		return database;
	}

	
	public void setDatabase(DataSourceFactory database) {
		this.database = database;
	}

	@Valid
	@NotNull
	@JsonProperty("database")
	private DataSourceFactory database = new DataSourceFactory();
	
}
