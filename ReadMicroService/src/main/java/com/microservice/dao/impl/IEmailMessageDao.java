package com.microservice.dao.impl;

import java.util.List;

import com.microservice.entity.EmailEntity;

public interface IEmailMessageDao {

	void create(EmailEntity eInput);
	
    List<EmailEntity> getEntityList(String queryString);
	
    EmailEntity read(int id);
    
    int getCount();
}
