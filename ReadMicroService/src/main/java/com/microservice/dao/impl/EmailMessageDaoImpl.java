package com.microservice.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.microservice.entity.EmailEntity;

@Singleton
public class EmailMessageDaoImpl implements IEmailMessageDao {

	@Inject
	private SessionFactory sessionFactory;

	public void create(EmailEntity eInput) {
		Session session = null;
		Transaction transaction = null;
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			session.save(eInput);
			session.flush();
			transaction.commit();

		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
		} finally {
			session.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<EmailEntity> getEntityList(String queryString) {

		Session session = sessionFactory.openSession();
		List<EmailEntity> e = null;
		try {
			e = session.createQuery(queryString).list();

		} catch (Exception exc) {
		} finally {
			session.close();
		}
		return e;
	}
	
	public EmailEntity read(int id) {

		Session session = sessionFactory.openSession();

		try {
			
			String hql = "FROM EmailEntity E WHERE E.id = " + id;
			Query query = session.createQuery(hql);
			
			return (EmailEntity)query.list().get(0);

		} catch (Exception e) {
			throw e;
		} finally {
			session.close();
		}
	}

	@Override
	public int getCount() {

		Session session = sessionFactory.openSession();
		
		try {
			
			return  session.createQuery("FROM EmailEntity").list().size();
		} catch (Exception e) {
			throw e;
		} finally {
			session.close();
		}
	}
}
