package com.microservice.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MessageResponse {

	private String messageText;
	private String messageStatus;
	
	@JsonProperty
	public String getMessageText() {
		return messageText;
	}
	
	@JsonProperty
	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}
	
	@JsonProperty
	public String getMessageStatus() {
		return messageStatus;
	}
	
	@JsonProperty
	public void setMessageStatus(String messageStatus) {
		this.messageStatus = messageStatus;
	}
	
	public MessageResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MessageResponse(String messageText, String messageStatus) {
		super();
		this.messageText = messageText;
		this.messageStatus = messageStatus;
	}
	
}
