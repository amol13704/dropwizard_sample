package com.microservice.service;

import java.util.List;

import com.microservice.entity.EmailEntity;
import com.microservice.response.MessageResponse;
import com.microservice.vo.IMessage;

public interface IMessageService {

	MessageResponse addMessage(IMessage message);
    List<EmailEntity> getAllMessages();
    EmailEntity getMessageForId(int id);
    int getCount();
}
