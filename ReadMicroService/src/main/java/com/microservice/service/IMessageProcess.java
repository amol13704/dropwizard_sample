package com.microservice.service;

import com.microservice.response.MessageResponse;
import com.microservice.vo.IMessage;

public interface IMessageProcess {

	MessageResponse processMessage(IMessage iMessage);
	
	
}
