package com.microservice.service;

import java.util.List;

import com.google.inject.Inject;
import com.microservice.dao.impl.IEmailMessageDao;
import com.microservice.entity.EmailEntity;
import com.microservice.response.MessageResponse;
import com.microservice.vo.EmailMessage;
import com.microservice.vo.IMessage;

public class EmailMessageService implements IMessageService {

	@Inject
	private IEmailMessageDao dao;

	public MessageResponse addMessage(IMessage message) {
		try {
			System.out.println("MessageText = " + ((EmailMessage) message).getMessageText());
			EmailEntity emailEntity = new EmailEntity();
			EmailMessage msg = (EmailMessage) message;
			emailEntity.setEmailAaddress(msg.getRecAddress());
			emailEntity.setId(msg.getId());
			emailEntity.setMsg(msg.getMessageText() + "EmailMesage to string = " + msg.toString()
			+ " HashCode = " + msg.hashCode());
			dao.create(emailEntity);
			return new MessageResponse("Email msg : " + (msg.getMessageText()), "OK");
		} catch (Exception ex) {

		}
		return null;
	}

	public List<EmailEntity> getAllMessages() {
		try {
			return dao.getEntityList("FROM EmailEntity");
		} catch (Exception ex) {

		}
		return null;
	}

	public EmailEntity getMessageForId(int id) {
		try {
			return dao.read(id);
		} catch (Exception ex) {

		}
		return null;

	}

	@Override
	public int getCount() {
		try {
			return dao.getCount();
		} catch (Exception ex) {

		}
		return 0;

	}
}
