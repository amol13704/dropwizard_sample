package com.microservice.service;

import com.microservice.response.MessageResponse;
import com.microservice.vo.IMessage;

public class MessageProcessImpl implements IMessageProcess {

	
	private IMessageService emailService;

	@Override
	public MessageResponse processMessage(IMessage iMessage) {
		// TODO Auto-generated method stub
		MessageResponse messageResponse = null;
		// if(iMessage.messageType().equals(Constants.MessageType.EMAIL)) {
		messageResponse = emailService.addMessage(iMessage);
		//// } else if(iMessage.messageType().equals(Constants.MessageType.SMS)) {
		// messageResponse = smsService.sendMessage(iMessage);
		// }
		return messageResponse;
	}
	
	

}
