package com.microservice.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "email_data")
public class EmailEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	@Column(name = "email_msg")
	private String msg;
	
	@Column(name = "email_address")
	private String emailAaddress;

	@JsonProperty
	public int getId() {
		return id;
	}

	@JsonProperty
	public void setId(int id) {
		this.id = id;
	}

	@JsonProperty
	public String getMsg() {
		return msg;
	}

	@JsonProperty
	public void setMsg(String msg) {
		this.msg = msg;
	}

	@JsonProperty
	public String getEmailAaddress() {
		return emailAaddress;
	}

	@JsonProperty
	public void setEmailAaddress(String emailAaddress) {
		this.emailAaddress = emailAaddress;
	}
}
