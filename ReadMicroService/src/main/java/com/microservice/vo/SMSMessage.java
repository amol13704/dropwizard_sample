package com.microservice.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.microservice.util.Constants.MessageType;

public class SMSMessage implements IMessage{

    private String messageText;
    private String recAddress;
	
	public SMSMessage() {
		super();
		// TODO Auto-generated constructor stub
	}

	@JsonProperty
	public String getMessageText() {
		return messageText;
	}

	@JsonProperty
	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}

	@JsonProperty
	public String getRecAddress() {
		return recAddress;
	}

	@JsonProperty
	public void setRecAddress(String recAddress) {
		this.recAddress = recAddress;
	}

	public String messageType() {
		// TODO Auto-generated method stub
		return MessageType.SMS.toString();
	}

}
