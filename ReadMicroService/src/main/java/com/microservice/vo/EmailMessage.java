package com.microservice.vo;


import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString
@EqualsAndHashCode
@Builder
public class EmailMessage implements IMessage {
 
	
	private String messageText; 
	private String recAddress;
	private int id;
	
	public EmailMessage getEmailMessage() {
	
		return new EmailMessageBuilder()
				.messageText("messageTeat")
				.recAddress("recAddress")
				.id(1).build();
	}
	
	@Override
	public String messageType() {
		// TODO Auto-generated method stub
		return null;
	}
}
