package com.microservice.resource;

import java.util.List;

import javax.validation.Valid;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.digitalabel.dto.AssignLabelEndPointResponseDTO;
import com.digitalabel.dto.AssignLabelRequestDTO;
import com.digitalabel.dto.ConfigureLabelEndPointRequestDTO;
import com.digitalabel.serviceImpl.AsignLabelServiceImpl;
import com.google.inject.Inject;
import com.microservice.entity.EmailEntity;
import com.microservice.response.MessageResponse;
import com.microservice.service.IMessageService;
import com.microservice.vo.EmailMessage;

@Path("/message")
@Produces(MediaType.APPLICATION_JSON)
public class ClientResource {
	
	@Inject
	private IMessageService service;
	
	@Inject
	private AsignLabelServiceImpl impl;
		
	@POST
	@Path("/emailmessage")
	public MessageResponse postEmailMessage(@Valid EmailMessage message) {
			
		return service.addMessage(message);
	}
	
	@POST
	@Path("/assignlabel")
	public AssignLabelEndPointResponseDTO assignLabel(@Valid AssignLabelRequestDTO assignLabelRequestDTO) {
			
		return impl.asignLabel(assignLabelRequestDTO, "read barcode from path param");
	}
	
	@POST
	@Path("/configurelabel")
	public Response configureLabel(@Valid ConfigureLabelEndPointRequestDTO configureLabelEndPointRequestDTO) {
			
		return impl.configureLabel(configureLabelEndPointRequestDTO, "read barcode from path param");
	}
	
	@GET
	@Path("/allmessage")
	public List<EmailEntity> getAllMessage() {
		return service.getAllMessages();
	}
	
	@GET
	@Path("/message/{id}")
	public EmailEntity getMessageForId(@PathParam("id") int id) {
		return service.getMessageForId(id);
	}
	
	@GET
	@Path("/messagecount")
	public int getMessageCount() {
		
		return service.getCount();
	}
}
