package com.microservice.module;

import javax.ws.rs.core.Response;

import org.hibernate.SessionFactory;

import com.digitalabel.dto.AssignLabelEndPointResponseDTO;
import com.digitalabel.dto.AssignLabelRequestDTO;
import com.digitalabel.dto.ConfigureLabelEndPointRequestDTO;
import com.digitalabel.endpoint.IBasePostEndPoint;
import com.digitalabel.endpoint.impl.AssignLabelEndPoint;
import com.digitalabel.endpoint.impl.ConfigureLabelEndPoint;
import com.google.inject.TypeLiteral;
import com.google.inject.name.Names;
import com.microservice.configuration.MicroserviceConfiguration;
import com.microservice.dao.impl.EmailMessageDaoImpl;
import com.microservice.dao.impl.IEmailMessageDao;
import com.microservice.service.EmailMessageService;
import com.microservice.service.IMessageProcess;
import com.microservice.service.IMessageService;
import com.microservice.service.MessageProcessImpl;

import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.hibernate.ScanningHibernateBundle;
import io.dropwizard.setup.Bootstrap;
import ru.vyarus.dropwizard.guice.module.support.DropwizardAwareModule;

public class ApplicationModule  extends DropwizardAwareModule<MicroserviceConfiguration>  {

	
	private final HibernateBundle<MicroserviceConfiguration> hibernateBundle = new ScanningHibernateBundle<MicroserviceConfiguration>(
			"com.microservice.entity") {
		
		@Override
		public DataSourceFactory getDataSourceFactory(MicroserviceConfiguration configuration) {
			return configuration.getDatabase();
		}
	};
	
	public ApplicationModule(Bootstrap<MicroserviceConfiguration> configBootstrap) {
		configBootstrap.addBundle(hibernateBundle);
	}
	
	@Override
	protected void configure() {
		//bind the service to implementation class
		 bind(SessionFactory.class).toInstance(hibernateBundle.getSessionFactory());
		 bind(IMessageService.class).to(EmailMessageService.class);
		 bind(IMessageProcess.class).to(MessageProcessImpl.class);
		 bind(IEmailMessageDao.class).to(EmailMessageDaoImpl.class);
		
		 bind(new TypeLiteral<IBasePostEndPoint<AssignLabelRequestDTO, AssignLabelEndPointResponseDTO>>() {
			}).annotatedWith(Names.named(AssignLabelEndPoint.ASSIGN_LABEL))
					.to(new TypeLiteral<AssignLabelEndPoint>() {
					});
		 
		 bind(new TypeLiteral<IBasePostEndPoint<ConfigureLabelEndPointRequestDTO, Response>>() {
			}).annotatedWith(Names.named(ConfigureLabelEndPoint.CONFIGURE_LABEL))
					.to(new TypeLiteral<ConfigureLabelEndPoint>() {
					});
		//bind(IMessageService.class).to(EmailMessageService.class);
	}
}
