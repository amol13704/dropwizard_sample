package com.digitalabel.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.digitalabel.dto.AssignLabelEndPointResponseDTO;
import com.digitalabel.dto.AssignLabelRequestDTO;
import com.digitalabel.dto.ConfigureLabelEndPointRequestDTO;
import com.digitalabel.endpoint.IBasePostEndPoint;
import com.digitalabel.endpoint.impl.AssignLabelEndPoint;
import com.digitalabel.endpoint.impl.ConfigureLabelEndPoint;
import com.digitalabel.endpoint.vo.ParameterMapping;
import com.digitalabel.endpoint.vo.ParameterMappings;
import com.google.common.net.HttpHeaders;
import com.google.inject.Inject;
import com.google.inject.name.Named;

//This class consumes the endpoint created by us. Which is AssignLabelEndPoint
public class AsignLabelServiceImpl {
	
	private IBasePostEndPoint<AssignLabelRequestDTO, AssignLabelEndPointResponseDTO> assignLabelEndPoint;
	
	private IBasePostEndPoint<ConfigureLabelEndPointRequestDTO, Response> configureLabelEndPoint;
	
	@Inject
	public AsignLabelServiceImpl(@Named(AssignLabelEndPoint.ASSIGN_LABEL) IBasePostEndPoint<AssignLabelRequestDTO, AssignLabelEndPointResponseDTO> assignLabelEndPoint, 
			@Named(ConfigureLabelEndPoint.CONFIGURE_LABEL)IBasePostEndPoint<ConfigureLabelEndPointRequestDTO, Response> configureLabelEndPoint) {
		// the endpoint will be injected at runtime with the help of Google guice
		this.assignLabelEndPoint = assignLabelEndPoint;
		this.configureLabelEndPoint = configureLabelEndPoint;
	}
	
	public AssignLabelEndPointResponseDTO asignLabel(AssignLabelRequestDTO assignLabelRequestDTO, String barcode) {
		//consume the endpount by calling the POST method
		// the post method send data as parameters
		//the post method will return the appropriate response
		return assignLabelEndPoint.post(getParameterMappings(barcode), assignLabelRequestDTO);
	}

	public Response configureLabel(ConfigureLabelEndPointRequestDTO assignLabelRequestDTO, String barcode) {
		//consume the endpount by calling the POST method
		// the post method send data as parameters
		//the post method will return the appropriate response
		return configureLabelEndPoint.post(getParameterMappings(barcode), assignLabelRequestDTO);
	}
	
	private ParameterMappings getParameterMappings( String barcod) {
		// TODO Auto-generated method stub
		// retuen all the parameters here
		// this will include Path Params, Query Params and Header params
		ParameterMappings mappings = new ParameterMappings();
		
		List<ParameterMapping> headerParams = new ArrayList<>();
		headerParams.add(new ParameterMapping(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON));
			
		List<ParameterMapping> pathParams = new ArrayList<>();
		pathParams.add(new ParameterMapping("locationName", "location"));
		pathParams.add(new ParameterMapping("displayId", barcod));
		
		List<ParameterMapping> queryParam = new ArrayList<>();
		queryParam.add(new ParameterMapping("autoConfigure", true));
		
		mappings.setHeaderParameters(headerParams);
		mappings.setPathParameters(pathParams);
		mappings.setQueryParameters(queryParam);
		
		return mappings;
	}
}
