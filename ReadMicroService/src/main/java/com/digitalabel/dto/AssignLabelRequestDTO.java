package com.digitalabel.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class AssignLabelRequestDTO {
	
 @JsonProperty("ScannedItem")	
 private String scannedItem;

 @JsonProperty("Sequence")
 private int sequence;

}
