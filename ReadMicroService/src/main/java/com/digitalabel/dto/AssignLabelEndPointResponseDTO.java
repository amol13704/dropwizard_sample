package com.digitalabel.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class AssignLabelEndPointResponseDTO {
	
	@JsonProperty("Key")	
	 private String key;
	
	@JsonProperty("Value")
	 private String value;
}