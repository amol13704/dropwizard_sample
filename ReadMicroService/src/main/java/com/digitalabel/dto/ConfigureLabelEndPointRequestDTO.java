package com.digitalabel.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ConfigureLabelEndPointRequestDTO {

	@JsonProperty
	private List<String> labelDisplayIdList;

	public List<String> getLabelDisplayIdList() {
		return labelDisplayIdList;
	}

	public void setLabelDisplayIdList(List<String> labelDisplayIdList) {
		this.labelDisplayIdList = labelDisplayIdList;
	}
}
