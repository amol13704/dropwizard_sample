package com.digitalabel.endpoint;

import com.digitalabel.endpoint.vo.ParameterMappings;

/**
 * 
 * @author surajv
 *
 * @param <I>
 *            Input structure
 * @param <O>
 *            Output Structure
 */
public interface IBasePostEndPoint<I, O> {

	O post(ParameterMappings parameterMappings, I input);
}
