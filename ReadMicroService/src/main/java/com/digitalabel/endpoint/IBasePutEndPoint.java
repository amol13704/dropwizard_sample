package com.digitalabel.endpoint;

import com.digitalabel.endpoint.vo.ParameterMappings;

public interface IBasePutEndPoint<I, O> {

	O put(ParameterMappings parameterMappings, I input);
}
