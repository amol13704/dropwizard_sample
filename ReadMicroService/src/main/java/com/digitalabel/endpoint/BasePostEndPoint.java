package com.digitalabel.endpoint;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.Response;

import com.digitalabel.config.ExternalServiceConfig;
import com.digitalabel.endpoint.vo.ParameterMappings;

/**
 * 
 * @author surajv
 *
 * @param <I>
 *            Input structure
 * @param <O>
 *            Output Structure
 */
public abstract class BasePostEndPoint<I, O> extends BaseEndPoint<I, O> implements IBasePostEndPoint<I, O> {

	public BasePostEndPoint(Client client, ExternalServiceConfig externalServiceConfig) {
		super(client, externalServiceConfig);
	}

	@Override
	public final O post(ParameterMappings parameterMappings, I input) {
		return send(parameterMappings, input);
	}

	@Override
	protected final Response getResponse(Invocation.Builder invocationBuilder, I input) {
		
		Entity<I> e = Entity.json(input);
		return invocationBuilder.post(e);
	}

	@Override
	public boolean isStatusValid(int status) {
		return status == Response.Status.OK.getStatusCode();
	}
}
