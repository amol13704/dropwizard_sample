package com.digitalabel.endpoint;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.core.Response;

import com.digitalabel.config.ExternalServiceConfig;
import com.digitalabel.endpoint.vo.ParameterMappings;

public abstract class BasePutEndPoint<I, O> extends BaseEndPoint<I, O> implements IBasePutEndPoint<I, O>{

	public BasePutEndPoint(Client client, ExternalServiceConfig externalServiceConfig) {
		super(client, externalServiceConfig);
	}

	@Override
	protected Response getResponse(Builder invocationBuilder, I input) {
	
		Entity<I> e = Entity.json(input);
		return invocationBuilder.put(e);
	}
	
	@Override
	protected boolean isStatusValid(int status) {
		return status == Response.Status.OK.getStatusCode();
	}

	@Override
	public O put(ParameterMappings parameterMappings, I input) {
		return send(parameterMappings, input);
	}
}
