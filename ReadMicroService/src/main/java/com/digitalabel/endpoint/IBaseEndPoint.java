package com.digitalabel.endpoint;

import com.digitalabel.endpoint.vo.ParameterMappings;

/**
 * 
 * @author surajv
 *
 * @param <I>
 *            Input structure
 * @param <O>
 *            Output Structure
 */
public interface IBaseEndPoint<I, O> {

	O send(ParameterMappings parameterMappings, I input);
}
