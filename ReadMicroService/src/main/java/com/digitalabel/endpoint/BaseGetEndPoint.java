package com.digitalabel.endpoint;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.Response;

import com.digitalabel.config.ExternalServiceConfig;
import com.digitalabel.endpoint.vo.ParameterMappings;

/**
 * 
 * @author surajv
 *
 * @param <O>
 *            Output Structure
 */
public abstract class BaseGetEndPoint<O> extends BaseEndPoint<Object, O> implements IBaseGetEndPoint<O> {

	public BaseGetEndPoint(Client client, ExternalServiceConfig externalServiceConfig) {

		super(client, externalServiceConfig);
	}

	@Override
	public final O get(ParameterMappings parameterMappings) {
		return send(parameterMappings, null);
	}

	@Override
	protected final Response getResponse(Invocation.Builder invocationBuilder, Object input) {
		return invocationBuilder.get();
	}

	@Override
	public boolean isStatusValid(int status) {
		return status == Response.Status.OK.getStatusCode();
	}
}
