package com.digitalabel.endpoint;

import com.digitalabel.endpoint.vo.ParameterMappings;

/**
 * 
 * @author surajv
 *
 * @param <O>
 *            Output Structure
 */
public interface IBaseGetEndPoint<O> {

	O get(ParameterMappings parameterMappings);
}
