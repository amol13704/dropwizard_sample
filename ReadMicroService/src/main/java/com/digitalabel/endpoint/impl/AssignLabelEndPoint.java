package com.digitalabel.endpoint.impl;

import javax.ws.rs.client.Client;

import com.digitalabel.config.ExternalServiceConfig;
import com.digitalabel.dto.AssignLabelEndPointResponseDTO;
import com.digitalabel.dto.AssignLabelRequestDTO;
import com.digitalabel.endpoint.BasePostEndPoint;
import com.google.inject.Inject;

public class AssignLabelEndPoint extends BasePostEndPoint<AssignLabelRequestDTO, AssignLabelEndPointResponseDTO> {
	
	public static final String ASSIGN_LABEL = "assign label endpoint";
	
	//Inject the client and configuration in the constructor 
	@Inject
	public AssignLabelEndPoint(Client client, ExternalServiceConfig externalServiceConfig) {
		//pass the client and configuration to the super class
		super(client, externalServiceConfig);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Class<AssignLabelEndPointResponseDTO> getOutputEntityClass() {
		// TODO Auto-generated method stub
		//Child implementation will provide the specific output entity class
		return AssignLabelEndPointResponseDTO.class;
	}

	@Override
	protected RuntimeException getExceptionForErrorResponse(String message) {
		// TODO Auto-generated method stub
		// Child implementation will provide the specific error response
		// We will return our custom exception here
		// ExceptionFactory will give us instance of appropriate exception.
		return null;
	}
	
	@Override
	public boolean isStatusValid(int status) {
		// TODO Auto-generated method stub
		//check for the status here
		//If status is not valid specific to this child implemetation then
		//getExceptionForErrorResponse method will get called
		return super.isStatusValid(status);
	}
}