package com.digitalabel.endpoint.impl;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.Response;

import com.digitalabel.config.ExternalServiceConfig;
import com.digitalabel.dto.ConfigureLabelEndPointRequestDTO;
import com.digitalabel.endpoint.BasePostEndPoint;
import com.google.inject.Inject;

public class ConfigureLabelEndPoint extends BasePostEndPoint<ConfigureLabelEndPointRequestDTO, Response> {

	public static final String CONFIGURE_LABEL = "configure label endpoint";
	
	//Inject the client and configuration in the constructor 
	@Inject
	public ConfigureLabelEndPoint(Client client, ExternalServiceConfig externalServiceConfig) {
		//pass the client and configuration to the super class
		super(client, externalServiceConfig);
	}

	@Override
	protected Class<Response> getOutputEntityClass() {
		return Response.class;
	}

	@Override
	protected RuntimeException getExceptionForErrorResponse(String message) {
		// TODO Auto-generated method stub
		// Child implementation will provide the specific error response
		// We will return our custom exception here
		// ExceptionFactory will give us instance of appropriate exception.
		return null;
	}
	
	@Override
	public boolean isStatusValid(int status) {
		// TODO Auto-generated method stub
		//check for the status here
		//If status is not valid specific to this child implemetation then
		//getExceptionForErrorResponse method will get called
		return super.isStatusValid(status);
	}
}
