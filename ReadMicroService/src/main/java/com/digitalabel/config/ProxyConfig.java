package com.digitalabel.config;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ProxyConfig {

	@JsonProperty("useProxy")
	private boolean useProxy;

	@JsonProperty("proxyHost")
	private String proxyHost;

	@JsonProperty("proxyPort")
	private Integer proxyPort;

	@JsonProperty("proxyUserName")
	private String proxyUserName;

	@JsonProperty("proxyPassword")
	private String proxyPassword;

	public boolean isUseProxy() {
		return useProxy;
	}

	public void setUseProxy(boolean useProxy) {
		this.useProxy = useProxy;
	}

	public String getProxyHost() {
		return proxyHost;
	}

	public void setProxyHost(String proxyHost) {
		this.proxyHost = proxyHost;
	}

	public Integer getProxyPort() {
		return proxyPort;
	}

	public void setProxyPort(Integer proxyPort) {
		this.proxyPort = proxyPort;
	}

	public String getProxyUserName() {
		return proxyUserName;
	}

	public void setProxyUserName(String proxyUserName) {
		this.proxyUserName = proxyUserName;
	}

	public String getProxyPassword() {
		return proxyPassword;
	}

	public void setProxyPassword(String proxyPassword) {
		this.proxyPassword = proxyPassword;
	}
}
