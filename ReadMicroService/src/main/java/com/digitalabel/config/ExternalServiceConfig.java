package com.digitalabel.config;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ExternalServiceConfig {

	@JsonProperty("uri")
	private String uri;

	@JsonProperty("apiKey")
	private String apiKey;

	@JsonProperty("authorization")
	private String authorization;

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public String getAuthorization() {
		return authorization;
	}

	public void setAuthorization(String authorization) {
		this.authorization = authorization;
	}
}
