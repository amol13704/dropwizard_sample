package com.digitalabel.provider;

import javax.ws.rs.client.Client;

import org.glassfish.jersey.apache.connector.ApacheConnectorProvider;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.glassfish.jersey.client.JerseyClientBuilder;

import com.digitalabel.config.ProxyConfig;
import com.google.inject.Inject;
import com.google.inject.Provider;

/**
 * Provider class for javax.ws.rs.client.Client
 * 
 * @author surajv
 *
 */
public class JerseyClientProvider implements Provider<Client> {

	public static final String HTTP_SCHEME = "http";

	private final ProxyConfig proxyConfig;

	@Inject
	public JerseyClientProvider(ProxyConfig proxyConfig) {

		this.proxyConfig = proxyConfig;
	}

	@Override
	public final Client get() {

		ClientConfig clientConfig = new ClientConfig();

		if (proxyConfig.isUseProxy()) {

			String proxyURIFinal = HTTP_SCHEME + "://" + proxyConfig.getProxyHost() + ":" + proxyConfig.getProxyPort();

			clientConfig.property(ClientProperties.PROXY_URI, proxyURIFinal);
			clientConfig.property(ClientProperties.PROXY_USERNAME, proxyConfig.getProxyUserName());
			clientConfig.property(ClientProperties.PROXY_PASSWORD, proxyConfig.getProxyPassword());
			clientConfig.property(ClientProperties.SUPPRESS_HTTP_COMPLIANCE_VALIDATION, true);
			clientConfig.connectorProvider(new ApacheConnectorProvider());
		}

		return JerseyClientBuilder.newClient(clientConfig);
	}
}
