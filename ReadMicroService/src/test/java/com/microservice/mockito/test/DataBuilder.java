package com.microservice.mockito.test;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.digitalabel.endpoint.vo.ParameterMapping;
import com.digitalabel.endpoint.vo.ParameterMappings;


/**
 * 
 * @author surajv
 *
 */
public class DataBuilder {

	private static final Logger LOGGER = LoggerFactory.getLogger(DataBuilder.class);

	/*public static ApplicationConfig getConfig(String configFilePath) {

		try (InputStream in = ApplicationConfigLoader.class.getClassLoader().getResourceAsStream(configFilePath)) {

			return new Yaml().loadAs(in, ApplicationConfig.class);

		} catch (Exception e) {
			String msg = "Exception occurred while initializing configuration from config path : " + configFilePath;
			WMMException ie = new WMMException(ErrorCodes.APP_CONF_LOAD_ERR, msg, e);
			LOGGER.error(msg, ie);
			throw ie;
		}
	}
*/
	public static ParameterMappings getParameterMappings() {

		ParameterMapping pm = new ParameterMapping("X", "Y");

		List<ParameterMapping> lpm = new ArrayList<>();
		lpm.add(pm);

		ParameterMappings pms = new ParameterMappings();

		pms.setHeaderParameters(lpm);
		pms.setPathParameters(lpm);
		pms.setQueryParameters(lpm);

		return pms;
	}
}
