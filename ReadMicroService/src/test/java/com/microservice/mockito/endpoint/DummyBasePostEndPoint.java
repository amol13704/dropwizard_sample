package com.microservice.mockito.endpoint;

import javax.ws.rs.client.Client;

import com.digitalabel.config.ExternalServiceConfig;
import com.digitalabel.endpoint.BasePostEndPoint;
import com.google.inject.Inject;

/**
 * 
 * @author surajv
 *
 */
public class DummyBasePostEndPoint extends BasePostEndPoint<DummyIO, DummyIO> {

	@Inject
	public DummyBasePostEndPoint(Client client, ExternalServiceConfig externalServiceConfig) {
		super(client, externalServiceConfig);
	}

	@Override
	protected Class<DummyIO> getOutputEntityClass() {
		return DummyIO.class;
	}

	@Override
	protected RuntimeException getExceptionForErrorResponse(String message) {
		return new RuntimeException();
	}
}
