package com.microservice.mockito.endpoint;

import javax.ws.rs.client.Client;

import com.digitalabel.config.ExternalServiceConfig;
import com.digitalabel.endpoint.BaseGetEndPoint;
import com.google.inject.Inject;

/**
 * 
 * @author surajv
 *
 */
public class DummyBaseGetEndPoint extends BaseGetEndPoint<DummyIO> {

	@Inject
	public DummyBaseGetEndPoint(Client client, ExternalServiceConfig externalServiceConfig) {
		super(client, externalServiceConfig);
	}

	@Override
	protected Class<DummyIO> getOutputEntityClass() {
		return DummyIO.class;
	}

	@Override
	protected RuntimeException getExceptionForErrorResponse(String message) {
		return new RuntimeException();
	}
}
