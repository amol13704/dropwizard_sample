package com.microservice.mockito.endpoint.impl;

import javax.ws.rs.core.Response;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.digitalabel.endpoint.impl.AssignLabelEndPoint;


public class AssignLabelEndPointTest {

	@InjectMocks
	private AssignLabelEndPoint assignLabelEndPoint;

	/*@Mock
	private IWMMExceptionFactory exceptionFactory;

	@Mock
	private WMMException exception;

	@Before
	public void setUp() {

		Mockito.when(exceptionFactory.createException(Matchers.anyInt(), Matchers.anyString())).thenReturn(exception);
	}

	@Test
	public void testGetExceptionForErrorResponse() {
		Assert.assertEquals("WMMException is not matching", exception,
				assignLabelEndPoint.getExceptionForErrorResponse("X"));
	}*/

	@Test
	public void testGetOutputEntityClass() {
		Assert.assertTrue("Class is null", assignLabelEndPoint.getOutputEntityClass() instanceof Class);
	}

	@Test
	public void testisStatusValid() {

		Assert.assertNotNull(assignLabelEndPoint.isStatusValid(212));

	}

	
}
