package com.microservice.mockito.endpoint;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.core.Response;

import com.digitalabel.config.ExternalServiceConfig;
import com.digitalabel.endpoint.BaseEndPoint;
import com.google.inject.Inject;

/**
  */
public class DummyBaseEndPoint extends BaseEndPoint<DummyIO, DummyIO> {

	@Inject
	public DummyBaseEndPoint(Client client, ExternalServiceConfig externalServiceConfig) {
		super(client, externalServiceConfig);
	}

	@Override
	protected Response getResponse(Builder invocationBuilder, DummyIO input) {
		return invocationBuilder.get();
	}

	@Override
	protected boolean isStatusValid(int status) {
		return status == 1;
	}

	@Override
	protected Class<DummyIO> getOutputEntityClass() {
		return DummyIO.class;
	}

	@Override
	protected RuntimeException getExceptionForErrorResponse(String message) {
		return new RuntimeException();
	}
}
